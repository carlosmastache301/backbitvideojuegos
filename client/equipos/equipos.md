# Añadir un equipo

http://localhost/integradora/client/equipos/add.php
{
  "lider": "Alfredo",
  "numeroIntegrantes":"5",
  "nombreEquipo":"Fantasmones",
  "integrantes":"Tabla incorrecta, pero se"
}

# Eliminar un equipo

http://localhost/integradora/client/equipos/delete.php?idEquipo=2

# Obtener todos los equipos

http://localhost/integradora/client/equipos/getAll.php

# Obtener un equipo

http://localhost/integradora/client/equipos/getOne.php?idEquipo=1

# Actualizar un equipo

http://localhost/integradora/client/equipos/update.php
{
  "idEquipo": "1",
  "lider": "Alfredo",
  "numeroIntegrantes":"5",
  "nombreEquipo":"Fantasmones",
  "integrantes":"Tabla incorrecta, pero se arregla despues"
}
