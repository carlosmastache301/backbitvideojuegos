<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");


$json = file_get_contents('php://input'); // RECIBE EL JSON DE ANGULAR

$params = json_decode($json); // DECODIFICA EL JSON Y LO GUARADA EN LA VARIABLE

require "../config/conexion.php";  // IMPORTA EL ARCHIVO CON LA CONEXION A LA DB

// REALIZA LA QUERY A LA DB
$resultado = mysqli_query($conexion, "SELECT * FROM `usuarios` WHERE `usuario`='".$params->usuario."'");
while($row = mysqli_fetch_array($resultado)){
    $nombre = $row['nombre'];
    $apellidos = $row['apellidos'];
    $contra = $row['contrasenia'];
    $idUser = $row['idUser'];
    $tipoUser = $row['tipoUser'];
    }

    // GENERA LOS DATOS DE RESPUESTA
    class Result {}
    $response = new Result();

    if(($resultado->num_rows > 0) && (password_verify($params->contrasenia,$contra))) {
        $response->resultado = 'OK';
        $response->mensaje = 'Bienvenido';
        $response->nombre = $nombre;
        $response->apellidos = $apellidos;
        $response->usuario = $params->usuario;
        $response->id = $idUser;
        $response->tipo = $tipoUser;
    } else {
        $response->resultado = 'FAIL';
        $response->mensaje = 'Datos incorrectos';
    }

    header('Content-Type: text/html');

    echo json_encode($response); // MUESTRA EL JSON GENERADO
?>
