# Añadir un equipo

http://localhost/integradora/admin/torneos/add.php{
{
  "nombreTorneo": "VCT",
  "idVideojuego": "1",
  "encargado":"Jbaird Games",
  "recompensa": "1,000 pesos",
  "descripcion": "VCT Valorant Champions 2023"
}

# Eliminar un equipo

http://localhost/integradora/admin/horarios/delete.php?idHorario=6

# Obtener todos los equipos

http://localhost/integradora/admin/horarios/getAll.php

# Obtener un equipo

http://localhost/integradora/admin/horarios/getOne.php?idHorario=1

# Actualizar un equipo

http://localhost/integradora/admin/horarios/update.php{
{
  "idHorario": "1",
  "fecha": "2022-10-24",
  "hora":"01:34:00"
}
