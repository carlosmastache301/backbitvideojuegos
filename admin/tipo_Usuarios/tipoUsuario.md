# Añadir un tipo de usuario

http://localhost/integradora/admin/tipo_usuarios/add.php
{
  "tipoUser": "Usuarios"
}

# Eliminar un tipo de usuario

http://localhost/integradora/admin/tipo_usuarios/delete.php?idTipoUser=3

# Obtener todos los tipos de usuarios

http://localhost/integradora/admin/tipo_usuarios/getAll.php

# Obtener un tipo de usuario

http://localhost/integradora/admin/tipo_usuarios/getOne.php?idTipoUser=1

# Actualizar un tipo de usuario

http://localhost/integradora/admin/tipo_usuarios/update.php
{
  "idTipoUser": "2",
  "tipoUser": "Usuario"
}
