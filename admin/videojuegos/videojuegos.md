# Añadir un videojuego

http://localhost/integradora/admin/videojuegos/add.php
{
  "nombreVideojuego":"Valorant",
  "modalidad":"FPS",
  "descripcion":"Shooter Táctico"
}

# Eliminar un videojuego

http://localhost/integradora/admin/videojuegos/delete.php?idVideojuego=2

# Obtener todos los videojuegos

http://localhost/integradora/admin/videojuegos/getAll.php

# Obtener un videojuego

http://localhost/integradora/admin/videojuegos/getOne.php?idVideojuego=1

# Actualizar un videojuego

http://localhost/integradora/admin/videojuegos/update.php
{
  "idVideojuego": "1",
  "nombreVideojuego":"Valorant",
  "modalidad":"FPS",
  "descripcion":"Shooter Táctico"
}
